﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>


// Maximum queue size
int queueSize;

// Number of items in input file
int itemsNumber;

//It contains the current number of elements in the queue
int numberItemsInQueue = 0;

int check;

FILE* outputFile = NULL;

typedef struct ItemData item;
item* items;


// Structure of our items
typedef struct ItemData {
	int itemId;
	int produceTime;
	int consumeTime;
	int priority;
	item* next;
} item;

// Structure of our PriorityQueue
struct queue
{
	item *head;
	pthread_mutex_t lock;
	pthread_cond_t emptyCond;
	pthread_cond_t fullCond;
};

// Creating queue
struct queue * queue_create()
{
	printf("queue create: \n");
	struct queue *newQueue = (typeof(newQueue))malloc(sizeof(*newQueue));
	newQueue->head = NULL;
	pthread_mutex_init(&newQueue->lock, 0);
	pthread_cond_init(&newQueue->emptyCond, 0);
	pthread_cond_init(&newQueue->fullCond, 0);
	return newQueue;
}

// Close queue
void queue_close(struct queue *queue)
{
	printf("queue close: \n");
	volatile struct queue *q = queue;

	pthread_mutex_lock(&queue->lock);
	printf("Locked \n");

	pthread_cond_broadcast(&queue->emptyCond);
	pthread_cond_broadcast(&queue->fullCond);
	pthread_mutex_unlock(&queue->lock);
	printf("Unlocked \n");
}

// Push element to priority queue
void enqueue(struct queue *queue, struct ItemData *newItem)
{
	printf("enqueue: \n");
	volatile struct queue *q = queue;

	// lock
	pthread_mutex_lock(&queue->lock);
	printf("Locked enqueue \n");

	if (numberItemsInQueue >= queueSize){
		// if there are more items, but our queue is full, we start to wait
		while (numberItemsInQueue >= queueSize){
			pthread_cond_wait(&queue->fullCond, &queue->lock);
		}
	}
	//if in the queue of elements less than the size of the queue, we add an element
	if (numberItemsInQueue<queueSize){

		if (q->head == NULL) {
			printf("+++ %d \n", newItem->itemId);
			numberItemsInQueue++;
			q->head = newItem;
			pthread_cond_broadcast(&queue->emptyCond);
			pthread_mutex_unlock(&queue->lock);
			return;
		}

		item *temp = (typeof(temp))malloc(sizeof(*temp));
		if (newItem->priority >= q->head->priority) {
			item *t = q->head;
			while (1) {
				if (t->next == NULL || (newItem->priority > t->priority && newItem->priority < t->next->priority)) {
					break;
				}
				t = t->next;
			}
			newItem->next = t->next;
			t->next = newItem;
			numberItemsInQueue++;
		}
		else {
			item *temp = q->head;
			q->head = newItem;
			q->head->next = temp;
			numberItemsInQueue++;
		}

	}
	printf("+++ %d \n", newItem->itemId);
	//printf("numberItemsInQueue=%d\n", numberItemsInQueue);
	pthread_mutex_unlock(&queue->lock);
	printf("Unlocked enqueue \n");
}

// Pop element from the priority queue
item * dequeue(struct queue *queue)
{
	printf("deque: \n");

	//shall unblock all threads currently blocked on the this condition
	if (numberItemsInQueue < queueSize)	pthread_cond_broadcast(&queue->fullCond);

	volatile struct queue *q = queue;

	pthread_mutex_lock(&queue->lock);
	printf("Locked deque \n");

	item *returningItem = (typeof(returningItem))malloc(sizeof(*returningItem));

	// if we havent items in the queue we start to wait
	if (numberItemsInQueue == 0){
		while (q->head == NULL && numberItemsInQueue == 0) {
			printf("Im waiting \n");
			pthread_cond_wait(&queue->emptyCond, &queue->lock);
		}
	}

	//get an item from the queue
	item *p = q->head;
	if (p) {
		returningItem->itemId = q->head->itemId;
		returningItem->produceTime = q->head->produceTime;
		returningItem->consumeTime = q->head->consumeTime;
		returningItem->priority = q->head->priority;
		q->head = q->head->next;
		numberItemsInQueue--;
		printf("Get number %d \n", returningItem->itemId);
	}
	else {
		return NULL;
	}

	printf("--- %d \n", returningItem->itemId);
	//printf("numberItemsInQueue=%d\n", numberItemsInQueue);
	pthread_mutex_unlock(&queue->lock);
	printf("Unlocked deque \n");

	return returningItem;
}

// Parse integers from string into array
void parseIntegers(int * input, char * line) {
	char *str = line;
	char *ptr = str;
	size_t i;
	for (i = 0; i < 4; i++) {
		input[i] = strtol(ptr, &ptr, 10);
	}
}

// Read all input items from the file
item * readFile(int * itemsNumber)
{
	printf("read file: \n");
	FILE * file;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	int count = 0;
	int input[4];

	file = fopen("items.txt", "r");
	if (file == NULL) {
		printf("Can\'t open the file (file items.txt is needed)");
		exit(1);
	}

	items = malloc(count * sizeof(item));
	while ((read = getline(&line, &len, file)) != -1) {
		items = realloc(items, (count + 1)*sizeof(item));
		parseIntegers(input, line);
		items[count].itemId = input[0];
		items[count].produceTime = input[1];
		items[count].consumeTime = input[2];
		items[count].priority = input[3];
		items[count].next = NULL;
		count++;
		printf("readed from file by line: %s \n", line);
	}

	printf("\n");
	fclose(file);

	if (line) free(line);

	*itemsNumber = count;
	return items;
}

// Write result to the file
void writeFile(struct ItemData * item)
{
	printf("Write file: \n");

	if (outputFile == NULL) {
		outputFile = fopen("results.txt", "w");
	}
	else {
		outputFile = fopen("results.txt", "a");
	}

	if (outputFile == NULL) {
		printf("Can\'t open the file (results.txt)");
		exit(1);
	}

	fprintf(outputFile, "%d ", item->itemId);
	fprintf(outputFile, "%d ", item->produceTime);
	fprintf(outputFile, "%d ", item->consumeTime);
	fprintf(outputFile, "%d ", item->priority);
	fprintf(outputFile, "\n");
	fclose(outputFile);
}
//consumer 
void * consumer_proccess(void *arg)
{
	printf("consumer_proccess: \n");
	struct queue *q = *((typeof(arg) *) arg);
	int count = 0;
	while (itemsNumber > count) {
		printf("CONSUMER WORKING \n");
		struct ItemData *i = dequeue(q);
		writeFile(i);
		sleep(i->consumeTime);
		count++;
	}
	return 0;
}

//producer
void * producer_proccess(void *arg)
{
	printf("producer proccess: \n");
	struct queue *q = *((typeof(arg) *) arg);
	int count = 0;
	while (itemsNumber > count) {
		printf("PRODUCER WORKING \n");
		enqueue(q, &items[count]);
		sleep(items[count].produceTime);
		count++;
	}
	printf("PRODUCER PROCESS BREAKED \n");
	return 0;
}

void producer(int socket) {
                FILE *reading;
                char hello[20];
                char *estr;
                printf("Open file:");
                reading = fopen("items.txt", "r");

                if(reading == NULL){printf("Error: open file\n");}
                else{printf("Open\n");}
                while(1)
                {

                    estr = fgets(hello,sizeof(hello),reading);
                    if(estr == NULL)
                    {
                        if(feof(reading) != 0)
                        {
                            printf("End file\n");
                            break;
                        }
                        else{
                            printf("Error: reading from file\n");
                            break;
                        }
                    }
                    check++;
                    write(socket, hello, sizeof(hello)); /* NB. this includes nul */
                }

                // write(socket, hello, sizeof(hello)); /* NB. this includes nul */
                /* go forth and do childish things with this end of the pipe */
            }

            void consumer(int socket) {
                /* do parental things with this end, like reading the child's message */
                char buf[1024];
                int i;
                // for(i = 0; i < check; i++){
                int n = read(socket, buf, sizeof(buf));
                    printf("parent received '%.*s'\n", n, buf);

            }

int main(int argc, char const *argv[])
{

	if (argc != 3) {
		printf("You entered an incorrect number of parameters!\n");
		exit(1);
	}
	else {
		queueSize = atoi(argv[1]);
		if (queueSize < 1 || queueSize > 4096) {
			printf("Queue size should be less than 4096 and more than 1!\n");
			exit(1);
		}
		if (strcmp(argv[2], "shmem") == 0) {
			//Shared memory (pthreads)

			// read inputs
			itemsNumber = 0;
			items = readFile(&itemsNumber);
			printf("number of input items is: %d \n", itemsNumber);

			// create our queue
			struct queue *queue = queue_create();

			printf("Queue was created \n");

			pthread_t consumer, producer;

			printf("Started to create thread producer proccess \n");

			// create pthreads
			pthread_create(&producer, NULL, producer_proccess, (void*)&queue);
			pthread_create(&consumer, NULL, consumer_proccess, (void*)&queue);

			pthread_join(producer, NULL);
			pthread_join(consumer, NULL);

			queue_close(queue);

			free(items);

		}
		else if (strcmp(argv[2], "socket") == 0) {
			// sockets

                int fd[2];
                static const int parentsocket = 0;
                static const int childsocket = 1;
                pid_t pid;

                /* 1. call socketpair ... */
                socketpair(PF_LOCAL, SOCK_STREAM, 0, fd);

                /* 2. call fork ... */
                
                pid = fork();
                int i;
                
                    if (pid == 0) { 
                        close(fd[parentsocket]);
                        consumer(fd[childsocket]);
                    } else { 
                        close(fd[childsocket]);
                        producer(fd[parentsocket]);                      
                    }
                                    
                exit(0);
		}
		else {
			printf("The  queue type (shmem or ​socket) is incorrect, check the spelling!\n");
			exit(1);
		}
	}


}
